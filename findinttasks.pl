#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;
use feature qw(say);

my $srcdir = $ARGV[0];
my @intfiles = <$srcdir/integrators/*.f90>;
my @taskfiles = <$srcdir/tasks/*.f90>;

open my $mk, '<', "Makefile";

my @mkfiles;
my $state = 1;
while (<$mk>) {
    if ($state == 0) {
        if (/^\s+\w/) {
            my $line = $_;
            $line =~ s/\.f90.*$/.f90/;
            $line =~ s/^\s+//;
            chomp $line;
            push @mkfiles, ($srcdir."/".$line);
        } else {$state = 1;}
    }
    $state = 0 if /SOURCES/;
    $state = 1 if /^$/;
}
close($mk);

my %substs;
$substs{integrators} = {};
$substs{tasks} = {};

# useful regexes
my $modmatch = qr/module +(?<mod>\w+)/;
my $itgmatch = qr/type, ?extends\(\w+\) *:: *(?<itg>\w+)/;
my $tasksubmatch = qr/subroutine +(?<name>\w+) *\((?<args>[^)]+)\)/;

sub itg_name { $_[0] =~ s/Int//r; }

my $begincodegen = qr/^
(?<align>\s*) ! \s*  <<<  # magic comment start
(?<subj>\w+) \s+
(?<type>\w+) \s*
(?:\((?<arglist>[^)]+)\))?    # optional args
/x;
my $endcodegen = qr/^((\s*)! *end>>>)/;

foreach my $i (@intfiles) {
    open my $fh, '<', $i or die "$!\n";
    grep /^$i/, @mkfiles or next;

    my $mod;
    while (<$fh>) {
        if (/$modmatch/) { $mod = $+{mod};} 
        if (/$itgmatch/) {
            my $itg = $+{itg};
            my $itgname = itg_name $itg;
            $substs{integrators}->{$itg} = {
                init => sub {"type($itg) :: itg_$itgname\n"},
                use  => sub {"use $mod\n"},
                sel  => sub {
                    my $args = join ",", @_; 
                    return <<"EOS"
case ("$itgname")
    itg_$itgname = $itg($args)
    call select_task(itg_$itgname, task_type)
EOS
                }
            };
        }
    }
    close $fh;
}

foreach my $t (@taskfiles) {
    open my $fh, '<', $t or die "$!\n";
    my $mod;
    while (<$fh>) {
        if (/$modmatch/) { $mod = $+{mod};} 
        if (/$tasksubmatch/) {
            my $sub = $+{name};
            my $declargs = $+{args};
            $substs{tasks}->{$sub} = {
                use  => sub {"use $mod\n"},
                sel  => sub {
                    my $args = join ",", @_; 
                    return <<"EOS"
case ("$sub")
    call $sub($args)
EOS
                }
            };
        }
    }
    close $fh;
}

open my $main, '<', "$srcdir/main.f90" or die $!;
$state = 0;
while(<$main>){
    if (/$begincodegen/) {
        print;
        my @args = exists $+{arglist} ? split /, */, $+{arglist} : (); 
        foreach my $k (keys %{$substs{$+{subj}}}) {
            print $+{align}, $substs{$+{subj}}->{$k}->{$+{type}}(@args);
        }
        $state = 1;
    }
    $state = 0 if /$endcodegen/;
    print if $state == 0;
}
close($main);
