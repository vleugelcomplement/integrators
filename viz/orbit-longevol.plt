#!/usr/bin/gnuplot

filename = 'test.dat'
nrows    = system(sprintf("wc -l %s | awk '{print $1}'",filename))
# system('mkdir -p pert')
# set samples 10000
#set term qt
set term pngcairo size 1000, 1000
set grid
set xlabel 'x'
set ylabel 'y'

set xrange[-2:2]
set yrange[-2:2]

do for [ii=1:nrows] {
    framename = sprintf("%s/proc%05d.png",gifdir,ii)
    set output framename
    set size 1,0.8
    set multiplot layout 2,1 scale 1,1
    set rmargin 10
    set lmargin 10
    set size 1,0.5
    set origin 0,0.4
    plot filename every ::ii::ii using 2:3 with points pointtype 7 pointsize 1 lc 1 title "1",\
         filename every ::ii::ii using 4:5 with points pointtype 7 pointsize 1 lc 2 title "2",\
         filename every ::1::ii using 2:3 with lines lc 1 notitle, \
         filename every ::1::ii using 4:5 with lines lc 2 notitle
    set xrange[0:10]
    set yrange[-1.987333334:-1.987333333]
    set size 1,0.3
    set origin 0,0
    plot filename every ::1::ii using 1:6 with lines title "energy"
    #
    unset multiplot
}
# system ("cd ".gifdir."; ffmpeg -r 50 -i 'proc%05d.png' video.mp4 ")

# vim:sw=4
