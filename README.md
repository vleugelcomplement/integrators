# integrators

Object-oriented framework to solve few-body problems with various integrators.

Provided `main.f90` file may serve as an usage example of this (intended to be) library.

You may test it in the following way
```sh
$ make
$ ./integrators RungeKutta print_solution input/binary.in >test.dat
$ gnuplot viz/orbit-energy.plt
``` 
and then explore `img` subdirectory
