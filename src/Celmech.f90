module Celmech
    use IntData
    use Utils
    use IO_array
    use Positions
    implicit none
contains 
        ! x = (p, q)
        ! p' = -dH/dq 
        ! q' =  dH/dp
        !
        !       p_1^2         G M_i
        ! H_1 = -----  -  Σ ---------
        !        2m_1        |q_{1i}|
        function fast_motion_eq(t, x) result(dx)
            real(mpc), intent(in) :: t
            type(Pos), intent(in) :: x
            type(Pos) :: dx
            real(mpc) :: R_abs, R_ij(spcdim)
            real(mpc), dimension(size(x%p())) :: p,q, dp
            integer   :: i,j

            p = x%p()
            q = x%q()
            dp = 0
            
            dx = Pos([dp, dp]) ! много нулей

            call dx%Mq([ ((p(ci(i,j))/m(i), j=1,spcdim), i=1,Nbodies) ])

            do i = 1, Nbodies
                do j = 1, Nbodies
                    R_ij = q(ci(i,1):ci(i,spcdim)) - q(ci(j,1):ci(j,spcdim))
                    R_abs = sqrt(sum(R_ij**2))
                    if (j /= i) dp(ci(i,1):ci(i,spcdim)) = dp(ci(i,1):ci(i,spcdim)) - m(i)*m(j)*R_ij/R_abs**3 
                end do
            end do
            call dx%Mp(dp)
        contains
            ! 2 -- тело
            ! 3 -- координаты
            pure function ci(body,coord) result(i)
                integer, intent(in) :: body, coord
                integer :: i
                i = (body-1)*spcdim + (coord-1) + 1
            end function ci
        end function fast_motion_eq
        
        function hamiltonian(t, x) result(H)
            real(mpc), intent(in) :: t
            type(Pos), intent(in) :: x

            real(mpc) :: H, kin, pot
            integer :: i,j
            real(mpc) :: R_abs(Nbodies), R_cur(Nbodies,spcdim)
            real(mpc), dimension(size(x%p())) :: p,q, dp

            H = 0; kin = 0; pot = 0
            p = x%p()
            q = x%q()
            do i = 1, Nbodies
                kin = kin + sum(p(ci(i,1):ci(i,spcdim))**2)/m(i)/2
                do j = 1, Nbodies
                    R_cur(j,:) = q(ci(i,1):ci(i,spcdim)) - q(ci(j,1):ci(j,spcdim))
                    R_abs(j) = norm2(R_cur(j,:))
                end do
                do j = 1, Nbodies
                    if (j /= i) pot = pot + m(i)*m(j)/R_abs(j)
                end do
            end do
            pot = pot/2
            H = kin - pot
        contains
            ! 2 -- тело
            ! 3 -- координаты
            pure function ci(body,coord) result(i)
                integer, intent(in) :: body, coord
                integer :: i
                i = (body-1)*spcdim + (coord-1) + 1
            end function ci
        end function hamiltonian
end module Celmech
