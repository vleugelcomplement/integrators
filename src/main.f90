program main
    use IntData
    use Const
    use Positions
    use Celmech
    use Integrators
    ! <<<integrators use
    use GenericSingleStep
    use Yoshida
    use Heun
    use Runge
    use LeapFrog
    ! end>>>
    ! <<<tasks use
    use ODEsolve
    use ODEsolve
    use ODEsolve
    ! end>>>
    implicit none

    integer   :: fd_data = 10, fd2 = 11, i

    character(len=36) :: itg_type, task_type, datafile

    call getarg(1, itg_type)
    call getarg(2, task_type)
    call getarg(3, datafile)
  
    if (trim(itg_type) .eq. "") itg_type = "fallback"
    if (trim(task_type) .eq. "") task_type = "fallback"
    if (trim(datafile) .eq. "") datafile = "/dev/stdin"
    
    open(fd_data, file=datafile, action="read")
    call read_data(fd_data)
    close(fd_data)
    call dispatch(fast_motion_eq, itg_type, task_type)

contains
    subroutine dispatch(f, itg_type, task_type)
        procedure(int_f) :: f
        character(len=36) :: itg_type
        integer, dimension(10) :: itgpars
        character(len=36) :: task_type
        type(Pos) :: inipos
        ! <<<integrators init
        type(GenericSingleStepInt) :: itg_GenericSingleStep
        type(YoshidaInt) :: itg_Yoshida
        type(HeunInt) :: itg_Heun
        type(RungeKuttaInt) :: itg_RungeKutta
        type(LeapfrogInt) :: itg_Leapfrog
        ! end>>>

        inipos = Pos(p0, q0)
        select case (itg_type)
        ! <<<integrators sel (f,t0,inipos,h)
        case ("GenericSingleStep")
    itg_GenericSingleStep = GenericSingleStepInt(f,t0,inipos,h)
    call select_task(itg_GenericSingleStep, task_type)
        case ("Yoshida")
    itg_Yoshida = YoshidaInt(f,t0,inipos,h)
    call select_task(itg_Yoshida, task_type)
        case ("Heun")
    itg_Heun = HeunInt(f,t0,inipos,h)
    call select_task(itg_Heun, task_type)
        case ("RungeKutta")
    itg_RungeKutta = RungeKuttaInt(f,t0,inipos,h)
    call select_task(itg_RungeKutta, task_type)
        case ("Leapfrog")
    itg_Leapfrog = LeapfrogInt(f,t0,inipos,h)
    call select_task(itg_Leapfrog, task_type)
        ! end>>>
        case default
            write(stderr, *) '"' // trim(itg_type) // '" '// "integrator not implemened"
            stop "NOT IMPLEMENTED"
        end select
    end subroutine dispatch


    subroutine select_task(itg, task_type)
        character(len=36) :: task_type
        class(Integrator) :: itg
        select case (task_type)
        ! <<<tasks sel (itg)
        case ("back_forw")
    call back_forw(itg)
        case ("test_steps")
    call test_steps(itg)
        case ("print_solution")
    call print_solution(itg)
        ! end>>>
        case default
            write(stderr, *) ('"' // trim(task_type) // '" ' // "task not implemened")
            stop "NOT IMPLEMENTED"
        end select
    end subroutine select_task


end program main
