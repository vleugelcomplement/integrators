module GenericSingleStep
    use Integrators
    implicit none

    type, extends(Integrator) :: GenericSingleStepInt
        type(Pos) :: c
        real(mpc) :: t
        procedure(gss_internal_step),pointer,nopass :: internal_step
    contains
        procedure :: set_inicond => gss_set_inicond
        procedure :: crewind     => gss_crewind
        procedure :: step        => gss_step
        procedure :: time        => gss_time
        procedure :: val         => gss_val
!         final :: clear_gss
    end type
    interface GenericSingleStepInt
        module procedure :: init_gss
    end interface GenericSingleStepInt

contains
! **************************
! generic integrator
! **************************
    subroutine gss_set_inicond(self, t0, c0)
        class(GenericSingleStepInt), intent(inout) :: self
        real(mpc), intent(in) :: t0
        type(Pos), intent(in) :: c0
        self%t0 = t0
        self%c0 = c0
    end subroutine gss_set_inicond

    
    subroutine gss_crewind(self)
        class(GenericSingleStepInt), intent(inout) :: self
        self%c = self%c0
        self%t = self%t0
    end subroutine gss_crewind
    
    
    function init_gss(f, t0, c0, step) result(self)
        type(Pos), intent(in) :: c0
        real(mpc), intent(in) :: step, t0
        type(GenericSingleStepInt) :: self
        procedure(int_f) :: f 

        integer :: dimen, fulldimen
        
        dimen = size(c0%p())
        fulldimen = size(c0%x())

        self%dimen = dimen
        self%has_cache = .false.
        self%f => f
        self%h = step
        self%internal_step => gss_internal_step

        call self%set_inicond(t0, c0)
        call self%crewind()

    end function init_gss
    
    
    function gss_time(self) result(t)
        class(GenericSingleStepInt), intent(in) :: self
        real(mpc) :: t
        t = self%t
    end function gss_time
    
    
    function gss_val(self) result(v)
        class(GenericSingleStepInt), intent(in) :: self
        type(Pos) :: v
        v = self%c
    end function gss_val
    
    
    function gss_internal_step(f,t,x,h) result(x1)
        procedure(int_f) :: f
        real(mpc), intent(in) :: h, t
        type(Pos), intent(in) :: x
        type(Pos) :: x1

        x1 = x
    end function gss_internal_step

    subroutine gss_step(self, h)
        class(GenericSingleStepInt), intent(inout) :: self
        real(mpc), optional, intent(in) :: h
        real(mpc) :: h_
        h_ = self%h
        if (present(h)) h_ = h
        
        self%c = self%internal_step(self%f,self%t, self%c, h_)
        self%t = self%t + h_
    end subroutine gss_step
    
end module GenericSingleStep

