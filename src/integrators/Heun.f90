module Heun
    use Integrators

    implicit none

    type, extends(Integrator) :: HeunInt
        type(Pos) :: c
        real(mpc) :: t
        real(mpc) :: ord = 2
    contains
        procedure :: set_inicond => heun_set_inicond
        procedure :: crewind     => heun_crewind
        procedure :: step        => heun_step
        procedure :: time        => heun_time
        procedure :: val         => heun_val
!         final :: clear_heun
    end type
    interface HeunInt
        module procedure :: init_heun
    end interface HeunInt
contains
! **************************
! Runge-Kutta integrator
! **************************
    subroutine heun_set_inicond(self, t0, c0)
        class(HeunInt), intent(inout) :: self
        real(mpc), intent(in) :: t0
        type(Pos), intent(in) :: c0
        self%t0 = t0
        self%c0 = c0
    end subroutine heun_set_inicond

    
    subroutine heun_crewind(self)
        class(HeunInt), intent(inout) :: self
        self%c = self%c0
        self%t = self%t0
    end subroutine heun_crewind
    
    
    function init_heun(f, t0, c0, step) result(self)
        type(Pos), intent(in) :: c0
        real(mpc), intent(in) :: step, t0
        type(HeunInt) :: self
        procedure(int_f) :: f 

        integer :: dimen, fulldimen
        
        dimen = size(c0%p())
        fulldimen = size(c0%x())

        self%dimen = dimen
        self%has_cache = .false.
        self%f => f
        self%h = step
        
        call self%set_inicond(t0, c0)
        call self%crewind()

    end function init_heun 
    
    
    function heun_time(self) result(t)
        class(HeunInt), intent(in) :: self
        real(mpc) :: t
        t = self%t
    end function heun_time
    
    
    function heun_val(self) result(v)
        class(HeunInt), intent(in) :: self
        type(Pos) :: v
        v = self%c
    end function heun_val
    
    
    function internal_heun_step(f, t, x, h) result(x1)
        procedure(int_f) :: f
        real(mpc), intent(in) :: h, t
        type(Pos), intent(in) :: x
        type(Pos) :: p, x1

        p = x + h*f(t, x)
        x1 = x + h/2 * (f(t, x) + f(t, p))
    end function internal_heun_step
    

    subroutine heun_step(self, h)
        class(HeunInt), intent(inout) :: self
        real(mpc), optional, intent(in) :: h
        real(mpc) :: h_
        h_ = self%h
        if (present(h)) h_ = h
       
        self%c = internal_heun_step(self%f,self%t, self%c, h_)
        self%t = self%t + h_
    end subroutine heun_step
    
end module Heun

