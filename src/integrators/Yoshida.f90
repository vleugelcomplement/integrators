module Yoshida
    use Leapfrog

    implicit none

    type, extends(GenericSingleStepInt) :: YoshidaInt
        integer :: ord
        real(mpc), allocatable, dimension(:) :: w
    contains
        procedure :: step => yoshida_step
    end type
    interface YoshidaInt
        module procedure :: init_yoshida
    end interface YoshidaInt

    real(mpc) :: ysteps4(2) = (/                      &
        -2.0_mpc**(1.0_mpc/3)/(2.0_mpc - 2.0_mpc**(1.0_mpc/3)),&
                1.0_mpc/(2.0_mpc - 2.0_mpc**(1.0_mpc/3)) &
    /)
    
    real(mpc) :: ysteps6(3) = (/&
       -0.117767998417887E1_mpc, &
        0.235573213359357E0_mpc, &
        0.784513610477560E0_mpc  &
    /)

    real(mpc) :: ysteps8(7) = (/&
        0.102799849391985E0_mpc, &
       -0.196061023297549E1_mpc, &
        0.193813913762276E1_mpc, &
       -0.158240635368243E0_mpc, &
       -0.144485223686048E1_mpc, &
        0.253693336566229E0_mpc, &
        0.914844246229740E0_mpc  &
    /)
contains
   
    function init_yoshida(f, t0, c0, step, ord) result(self)
        procedure(int_f) :: f
        type(Pos), intent(in) :: c0
        real(mpc), intent(in) :: step, t0
        integer, optional :: ord
        type(YoshidaInt) :: self

        integer :: ord_
        ord_ = 8
        if (present(ord)) ord_ = ord
        self%dimen = size(c0%p())
        self%has_cache = .false.
        self%f => f
        self%h = step
        self%ord = ord_
        allocate(self%w(2**(ord_/2-1)-1))
        select case (ord_)
        case (4)
            self%w = ysteps4
        case (6)
            self%w = ysteps6
        case (8)
            self%w = ysteps8
        case default
            stop "NOT IMPLEMENTED"
        end select 

        call self%set_inicond(t0, c0)
        call self%crewind()
    end function init_yoshida

    
    subroutine yoshida_step(self,h)! result(x1)
        class(YoshidaInt), intent(inout) :: self
        real(mpc), optional, intent(in) :: h
        real(mpc) :: h_

        type(Pos) :: x1 ! in middlestep

        real(mpc) :: w0
        integer :: N, i
        
        h_ = self%h
        if (present(h)) h_ = h

        associate (w => self%w, t => self%t, x=>self%c)
        N = 2**(self%ord/2-1)-1
        w0 = 1 - 2*sum(w)
        
        x1 = x
        do i = 1, N
            x1 = internal_lf_kdk(self%f,t,x1,h_*w(N-i+1))
        end do
        x1 = internal_lf_kdk(self%f,t,x1,h_*w0)
        do i = 1, N
            x1 = internal_lf_kdk(self%f,t,x1,h_*w(i))
        end do
        end associate
        
        self%c = x1
        self%t = self%t + h_
    end subroutine yoshida_step
    

end module Yoshida
