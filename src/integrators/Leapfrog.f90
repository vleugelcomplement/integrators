module LeapFrog
    use GenericSingleStep
    implicit none
    
    type, extends(GenericSingleStepInt) :: LeapfrogInt
        integer :: ord = 2
    end type
    interface LeapfrogInt
        module procedure :: init_lf
    end interface LeapfrogInt
   
contains
    function init_lf(f,t0, c0,step) result(self)
        type(Pos), intent(in) :: c0
        real(mpc), intent(in) :: step, t0
        type(LeapfrogInt) :: self
        procedure(int_f) :: f

        self%dimen = size(c0%p())
        self%has_cache = .false.
        self%f => f
        self%h = step
        self%internal_step => internal_lf_kdk

        call self%set_inicond(t0, c0)
        call self%crewind()
    end function init_lf


    function internal_lf_kdk(f,t,x,h) result(x1)
        procedure(int_f) :: f
        real(mpc), intent(in) :: h, t
        type(Pos), intent(in) :: x
        type(Pos) :: x_half, x1 ! in middlestep
        
        x1 = x
        x_half = x1 + h/2 * f(t, x1)
        call x1%Mp(x_half%p()) ! v_1/2
        x_half = x1 + h   * f(t+h/2, x1)
        call x1%Mq(x_half%q()) ! x_1
        x_half = x1 + h/2 * f(t+h,x1)
        call x1%Mp(x_half%p()) ! v_1
    end function internal_lf_kdk

end module LeapFrog
