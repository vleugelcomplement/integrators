module Runge
    use GenericSingleStep

    implicit none

    type, extends(GenericSingleStepInt) :: RungeKuttaInt
        integer :: ord = 4
    end type
    interface RungeKuttaInt
        module procedure :: init_runge
    end interface RungeKuttaInt
contains
! **************************
! Runge-Kutta integrator
! **************************
    function init_runge(f, t0, c0, step) result(self)
        type(Pos), intent(in) :: c0
        real(mpc), intent(in) :: step, t0
        type(RungeKuttaInt) :: self
        procedure(int_f) :: f

        integer :: dimen, fulldimen

        dimen = size(c0%p())
        fulldimen = size(c0%x())

        self%dimen = dimen
        self%has_cache = .false.
        self%f => f
        self%h = step
        self%internal_step => internal_runge_step

        call self%set_inicond(t0, c0)
        call self%crewind()

    end function init_runge


    function internal_euler_step(f, t, x, h) result(x1)
        procedure(int_f) :: f
        real(mpc), intent(in) :: h, t
        type(Pos), intent(in) :: x
        type(Pos) :: x1

        x1 = x + h * f(t, x)
    end function internal_euler_step
    
    
    function internal_sym_euler_step(f, t, x, h) result(x2)
        procedure(int_f) :: f
        real(mpc), intent(in) :: h, t
        type(Pos), intent(in) :: x
        type(Pos) :: x1, x2

        x1 = x + h * f(t, x)
        call x1%Mq(x%q())
        x2 = x1 + h * f(t, x1)
        call x2%Mp(x1%p())
    end function internal_sym_euler_step


    function internal_runge_step(f, t, x, h) result(x1)
        procedure(int_f) :: f
        real(mpc), intent(in) :: h, t
        type(Pos), intent(in) :: x
        type(Pos) :: x1, k(4)

        k(1) = h*f(t, x) 
        k(2) = h*f(t + h*0.5_mpc, x + 0.5_mpc*k(1)) 
        k(3) = h*f(t + h*0.5_mpc, x + 0.5_mpc*k(2)) 
        k(4) = h*f(t + h, x + k(3)) 
        x1 = x + 1.0_mpc/6.0_mpc * & 
            (k(1) + 2.0_mpc*k(2) + 2.0_mpc*k(3) + k(4))
    end function internal_runge_step
    
end module Runge

