module Integrators
    use Const
    use Positions
    implicit none
   
    type, abstract :: Integrator
        integer :: dimen
        logical :: has_cache
        
        type(Pos) :: c0
        real(mpc) :: t0
        real(mpc) :: h
        procedure(int_f), pointer, nopass :: f
        contains
            procedure(int_set_inicond) , deferred :: set_inicond
            procedure(int_crewind)     , deferred :: crewind
            procedure(int_step)        , deferred :: step
            procedure(int_time)        , deferred :: time
            procedure(int_val)         , deferred :: val
    end type
    abstract interface 
        function int_f(t, c) result(f)
            import :: mpc, Pos
            real(mpc), intent(in) :: t
            type(Pos), intent(in) :: c
            type(Pos) :: f
        end function int_f


        subroutine int_set_inicond(self, t0, c0)
            import Integrator,mpc, Pos
            class(Integrator), intent(inout) :: self
            real(mpc), intent(in) :: t0
            type(Pos), intent(in) :: c0
        end subroutine int_set_inicond
        
        
        subroutine int_crewind(self)
            import Integrator, mpc
            class(Integrator), intent(inout) :: self
        end subroutine int_crewind
        
        
        subroutine int_step(self,h)
            import Integrator,mpc
            class(Integrator),intent(inout) :: self
            real(mpc), optional, intent(in) :: h
        end subroutine int_step
        
        
        function int_time(self) result(t)
            import Integrator,mpc
            class(Integrator), intent(in) :: self
            real(mpc) :: t
        end function int_time
        
        
        function int_val(self) result(v)
            import Integrator,mpc, Pos
            class(Integrator), intent(in) :: self
            type(Pos) :: v
        end function int_val
    end interface  
contains
end module Integrators

