module ODEsolve
    use Positions
    use IO_array
    use Integrators
    use Celmech
    implicit none
    contains
#define prompt_read(v, s) write(stderr,"(a)",advance="no") "v ";read(*,*, iostat=s) v
#define check_read(v,v_, s) if(present(v)) then;v_=v;else;prompt_read(v_,s);endif

        subroutine print_solution(integ, t1, outstep)
            class(Integrator) :: integ
            real(mpc), optional :: t1 
            integer  :: fd = stdout
            real(mpc), optional :: outstep

!             real(mpc) :: cache(integ%ord, size(integ%x0)), v(size(integ%x0)),&
!                 &v1(size(integ%x0)), t
            type(Pos) :: v
            integer :: i,N, k, ios
            real(mpc) :: t, trange, qq(integ%dimen), t1_, os_

            check_read(t1,t1_, ios)
            check_read(outstep,os_, ios)

            if (os_ < integ%h) os_ = integ%h
            N=int((t1_-integ%time())/integ%h)
            
            call integ%crewind()
!             cache = integ%get_cache()
            
!             do i = 1, integ%cache_size-1
!
!                 v = cache(i,:)
!                 if (present(transf)) v(1:tDim) = transf(v(1:tDim))
!                 write(fd,*) integ%t0 + (i-1)*integ%h, v
!             end do
            trange = eps
            k = 0
            do i = 0, N-1
                t = integ%time()
                if (abs(k*os_ - t) < integ%h/2) then
                    v = integ%val(); 
                    qq = v%q()
                    write(fd,*) integ%time(), qq, hamiltonian(t, v)
                    k = k + 1
                end if
                call integ%step()
            end do
        end subroutine

        subroutine test_steps(integ, N, d)
            class(Integrator) :: integ
            integer :: N, N_, ios, i
            optional :: N,d
            real(mpc) :: d, d_, h

            check_read(N,N_,ios)
            check_read(d,d_,ios)

            h = integ%h
            do i = 1,N_
                h = h/d_
                integ%h = h
                call integ%crewind()
                call print_solution(integ,1.1*Period,Period)
            end do
        end subroutine

        subroutine back_forw(integ, t1)
            class(Integrator) :: integ
            real(mpc), optional :: t1 

            type(Pos) :: v
            integer :: i,N, k, ios
            real(mpc) :: t, trange, qq(integ%dimen), t1_, h

            check_read(t1,t1_, ios)
            h = integ%h
            N=int((t1_-integ%time())/h)
            
            call integ%crewind()
            v = integ%val(); 
            write(*,*) integ%time(), v%q(), hamiltonian(t, v)

            do i = 0, N-1
                call integ%step()
            end do
            
            v = integ%val(); 
            write(*,*) integ%time(), v%q(), hamiltonian(t, v)
            
            do i = 0, N-1
                call integ%step(-h)
            end do
            
            v = integ%val(); 
            write(*,*) integ%time(), v%q(), hamiltonian(t, v)
            
        end subroutine
end module ODEsolve
