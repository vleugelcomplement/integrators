#include "macros.f90"
module IntData
    use Const

    public
    integer :: spcdim
    integer :: Nbodies
    real(mpc), dimension(:), allocatable :: m
    real(mpc) :: t0
    real(mpc) :: Period
    real(mpc) :: h
    real(mpc), dimension(:), allocatable :: p0
    real(mpc), dimension(:), allocatable :: q0
    
    character(*), parameter, private :: descr = '(9x)'
contains
#define skipread(fd, var) read(fd, descr, advance="no"); read(fd, *) var
    subroutine read_data(fd)
        integer, intent(in) :: fd
        skipread(fd, spcdim)
        skipread(fd, Nbodies)
        
        allocate(m(Nbodies))
        skipread(fd, m)
           
        skipread(fd, t0)
        skipread(fd, Period)
        skipread(fd, h)
        
        allocate(p0(Nbodies*spcdim))
        skipread(fd, p0)
        
        allocate(q0(Nbodies*spcdim))
        skipread(fd, q0)
    end subroutine
end module IntData

